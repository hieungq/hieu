import UIKit

class AppNavigationBar: UIView {
    
    var theme: Themeable? {
        return ServiceFacade.getService(DefaultTheme.self)
    }
    let backButton = UIButton()
    let rightButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = theme?.navigationBackgroundColor
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setupTitleLabel(title: String) {
        let titleLabel = UILabel()
        titleLabel.text = title
        addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = theme?.navigationTitleColor
        titleLabel.font = theme?.navigationBarTitleFont
        titleLabel.textAlignment = .center
        let naviTitleConstraint = [
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7)
        ]
        NSLayoutConstraint.activate(naviTitleConstraint)
    }
    
    func setupBackButton(for view: UIViewController, withAction action: Selector) {
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.setImage(theme?.navigationBackButtonImage, for: .normal)
        backButton.addTarget(view, action: action, for: .touchUpInside)
        addSubview(backButton)
        let backButtonConstraint = [
            backButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            backButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            backButton.widthAnchor.constraint(equalTo: backButton.heightAnchor),
            backButton.widthAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5)
        ]
        NSLayoutConstraint.activate(backButtonConstraint)
    }
    
    func setupRightButton(for view: UIViewController, withAction action: Selector) {
        rightButton.translatesAutoresizingMaskIntoConstraints = false
        rightButton.layer.cornerRadius = frame.height * 2 / 6
        rightButton.layer.borderWidth = 1.0
        rightButton.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor
        rightButton.addTarget(view, action: action, for: .touchUpInside)
        addSubview(rightButton)
        let naviButtonConstraint = [
            rightButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            rightButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            rightButton.widthAnchor.constraint(equalToConstant: frame.height * 2 / 3),
            rightButton.heightAnchor.constraint(equalTo: rightButton.widthAnchor)
        ]
        NSLayoutConstraint.activate(naviButtonConstraint)
    }
}
