import UIKit

extension UIColor {
    static var backgroundColor: UIColor = UIColor(named: "BackgroundColor") ?? UIColor()
    static var baseButtonColor: UIColor = UIColor(named: "MainButtonColor") ?? UIColor()
    static var versionTextColor: UIColor = UIColor(named: "VersionLabelColor") ?? UIColor()
    static var naviBarColor: UIColor = UIColor(named: "NaviColor") ?? UIColor()
    static var baseTextColor: UIColor = UIColor(named: "BaseTextColor") ?? UIColor()
    static var headerAlertColor: UIColor = UIColor(named: "HeaderAlertColor") ?? UIColor()
}
