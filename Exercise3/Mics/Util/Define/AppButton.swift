import UIKit

final class BaseButton: UIButton {

    @IBInspectable var titleText: String? {
        didSet {
            setTitle(titleText, for: .normal)
            setTitleColor(UIColor.white, for: .normal)
            titleLabel?.font = UIFont.appFontBold.withSize(17)
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        setupUI()
    }

    func setupUI() {
        clipsToBounds = true
        layer.cornerRadius = 10
        backgroundColor = .baseButtonColor
        
        layer.shadowColor = UIColor.baseButtonColor.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 20
        layer.shadowRadius = 10
        layer.masksToBounds = false
    }
}
