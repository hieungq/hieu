//
//  AppFont.swift
//  Exercise3
//
//  Created by NeoLab on 11/12/20.
//

import UIKit

extension UIFont {
    static var appFontRegular: UIFont = UIFont.customFont(type: FontType.regular, size: 17)
    static var appFontBold: UIFont = UIFont.customFont(type: FontType.bold, size: 17)
    static var appFontMedium: UIFont = UIFont.customFont(type: FontType.medium, size: 17)
}
//Noto Sans CJK JP Bold 17.0
extension UIFont {
    enum FontType: String {
        case regular = "NotoSansCJKjp-Regular"
        case medium = "NotoSansCJKjp-Medium"
        case bold = "NotoSansCJKjp-Bold"
    }
    
    static func customFont(type: FontType, size: CGFloat) -> UIFont {
        guard let customFont = UIFont(name: type.rawValue, size: size) else {
            fatalError("""
    Failed to load the "CustomFont-Light" font.
    Make sure the font file is included in the project and the font name is spelled correctly.
    """
            )
        }
        if #available(iOS 11.0, *) {
            return UIFontMetrics.default.scaledFont(for: customFont)
        } else {
            return customFont
        }
    }
}
