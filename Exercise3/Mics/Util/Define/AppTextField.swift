import UIKit

final class AppTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        setupUI()
    }

    func setupUI() {
        backgroundColor = .white
        
        layer.borderColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
        layer.borderWidth = 1
        
        layer.masksToBounds = false
        layer.shadowRadius = 3.0
        layer.shadowColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1).cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 1.0
        
        font = UIFont.appFontMedium.withSize(15)
        textColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
        
        let attributes = [
            NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1),
            NSAttributedString.Key.font: UIFont.appFontMedium.withSize(15)
        ] as [NSAttributedString.Key: Any]

        attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: attributes)

    }
    
    let padding = UIEdgeInsets(top: 0, left: 22, bottom: 0, right: 22)

    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override public func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
