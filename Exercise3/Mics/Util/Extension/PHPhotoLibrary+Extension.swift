import Photos
import UIKit

public extension PHPhotoLibrary {
    
    static func execute(controller: UIViewController,
                        onAccessHasBeenGranted: @escaping () -> Void,
                        onAccessHasBeenDenied: (() -> Void)? = nil) {
        
        let onDeniedOrRestricted = onAccessHasBeenDenied ?? {
            let alert = UIAlertController(
                title: "We were unable to load your album",
                message: "You can enable access in Privacy Settings",
                preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { _ in
                if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(settingsURL)
                }
            }))
            DispatchQueue.main.async {
                controller.present(alert, animated: true)
            }
        }
        
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .notDetermined:
            requestPhotosPermission(onAccessHasBeenGranted)
        case .denied, .restricted:
            onDeniedOrRestricted()
        case .authorized:
            onAccessHasBeenGranted()
        default:
            fatalError("PHPhotoLibrary::execute - \"Unknown case\"")
        }
    }
    
}

private func requestPhotosPermission(_ onAuthorized: @escaping (() -> Void)) {
    PHPhotoLibrary.requestAuthorization({ status in
        switch status {
        case .notDetermined:
            requestPhotosPermission(onAuthorized)
        case .denied, .restricted:
            break
        case .authorized:
            onAuthorized()
        default:
            fatalError("PHPhotoLibrary::execute - \"Unknown case\"")
        }
    })
}
