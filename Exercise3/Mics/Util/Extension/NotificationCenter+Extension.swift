//
//  NotificationCenter+Extension.swift
//  FinalProject
//
//  Created by NeoLab on 10/12/20.
//

import Foundation

extension NSNotification.Name {
    static let updateCartNotification = NSNotification.Name("CartUpdate")
    static let updateAvatarImage = NSNotification.Name("ImageUpdate")
}
