//
//  String+Extension.swift
//  FinalProject
//
//  Created by NeoLab on 10/20/20.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
