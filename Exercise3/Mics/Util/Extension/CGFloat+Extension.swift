import UIKit

extension UINavigationController {
    func getNavigationBarFrame(for view: UIViewController) -> CGRect {
        var naviFrame: CGRect = CGRect()
        let navigationBarFrame = view.navigationController?.navigationBar.frame ?? CGRect()
        let statusBarFrame: CGRect = UIApplication.shared.statusBarFrame
        
        naviFrame = statusBarFrame
        naviFrame.size.height = statusBarFrame.height + navigationBarFrame.height
        return naviFrame
    }
}
