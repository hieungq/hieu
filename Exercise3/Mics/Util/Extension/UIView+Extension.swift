import UIKit

extension UIView {
    
    public func loadViewFromNib() -> UIView? {
        let typeOfSelf = type(of: self)
        let bundle = Bundle(for: typeOfSelf)
        let nib = UINib(nibName: String(describing: typeOfSelf), bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    public static var nib: UINib {
        return UINib(nibName: self.className, bundle: nil)
    }
    
    class func instanceFromNib(nibName: String = UIView.nibName) -> UIView {
        guard let nibView = UINib(nibName: nibName, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? UIView else {
            fatalError("Can not load view form nibName")
        }
        return nibView
    }
    
    class func instanceFromNib<T: UIView>(viewWithClass name: T.Type) -> T? {
        guard let nibView = UINib(nibName: String(describing: name), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? UIView else {
            fatalError("Can not load view form nibName")
        }
        return nibView as? T
    }
    
    public static var nibName: String {
        return self.className
    }
    
    func noneAutoresizing() {
        self.translatesAutoresizingMaskIntoConstraints = false
    }
}

extension UIView {
    class func getAllSubviews<T: UIView>(from parenView: UIView) -> [T] {
         return parenView.subviews.flatMap { subView -> [T] in
             var result = getAllSubviews(from: subView) as [T]
             if let view = subView as? T { result.append(view) }
             return result
         }
     }

     class func getAllSubviews(from parenView: UIView, types: [UIView.Type]) -> [UIView] {
         return parenView.subviews.flatMap { subView -> [UIView] in
            var result = getAllSubviews(from: subView) as [UIView]
            for type in types where subView.classForCoder == type {
                result.append(subView)
                return result
            }
            return result
         }
     }

    func getAllSubviews<T: UIView>() -> [T] { return UIView.getAllSubviews(from: self) as [T] }
    func get<T: UIView>(all type: T.Type) -> [T] { return UIView.getAllSubviews(from: self) as [T] }
    func get(all types: [UIView.Type]) -> [UIView] { return UIView.getAllSubviews(from: self, types: types) }
}
