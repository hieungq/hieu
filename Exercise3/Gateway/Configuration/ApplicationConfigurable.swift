import UIKit
protocol ApplicationConfigurable {
    var applicationRoute: AppplicationRouteable? { get }
    func applicationRoute(from: UIWindow, initialViewController: UIViewController) -> AppplicationRouteable
    func setupSpecificConfig()
}
