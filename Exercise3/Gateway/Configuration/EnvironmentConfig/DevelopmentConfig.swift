import UIKit
class ApplicationDevelopmentConfiguration: ApplicationConfigurable {
    var applicationRoute: AppplicationRouteable?
    func applicationRoute(from window: UIWindow, initialViewController: UIViewController) -> AppplicationRouteable {
        if let concreteRoute = self.applicationRoute {
            return concreteRoute
        }
        let newApplicationRoute = AppplicationRoute(window: window, initialViewController: initialViewController)
        self.applicationRoute = newApplicationRoute
        return newApplicationRoute
    }
    func setupSpecificConfig() {
//       MPDebugLog.share.start()
    }
}
