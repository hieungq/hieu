import Swinject
import NeoNetworking

extension Container {
    static var `default` = Container()
}
class ServiceFacade {
    static let applicationService: ApplicationConfigurable = ApplicationDevelopmentConfiguration()
    static let apiService: NeoNetworkProviable = NeoNetworkProvider(with: APIConfigableSample())

    static let realmService = RealmStorateService()
    static private(set) var theme = DefaultTheme()
    
    static func registerDefaultService(from windown: UIWindow) {
        ServiceFacade.initializeService(from: windown)
    }
    static func getService<T>(_ type: T.Type) -> T? {
        return Container.default.resolve(type)
    }
    static private func initializeService(from window: UIWindow) {
        applicationService.setupSpecificConfig()
        Container.default.register(ApplicationConfigurable.self) { (_) in
            return ServiceFacade.applicationService
        }
        Container.default.register(RealmStorateService.self) { (_) in
            return ServiceFacade.realmService
        }
        Container.default.register(DefaultTheme.self) { (_) in
            return ServiceFacade.theme
        }
    }
}
