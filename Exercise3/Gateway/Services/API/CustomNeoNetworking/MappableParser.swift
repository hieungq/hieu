import Foundation
import NeoNetworking
import ObjectMapper

class MappableParser<ParsingType: Mappable>: Parseable {

func parse<T>(_ data: Data) -> T? {
    do {
        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
            return Mapper<ParsingType>().map(JSON: json) as? T
        }
    } catch let error as NSError {
        print("Failed to load: \(error.localizedDescription)")
    }
        return nil
    }
}
