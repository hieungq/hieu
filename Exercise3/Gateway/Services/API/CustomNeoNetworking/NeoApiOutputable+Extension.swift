//
//  NeoApiOutputable+Extension.swift
//  FinalProject
//
//  Created by NeoLab on 10/29/20.
//

import Foundation
import NeoNetworking
import Alamofire

public extension NeoApiOutputable {
    func convertError(from data: Data?, systemError: Error?) {
        guard let data = data else {
            self.error = nil
            return
        }
        self.error = nil
        if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
            guard let code = json["code"] as? Int else { return }
            if (200...400).contains(code) {
                return
            }
            self.error = errorParser.parse(data)
        }
    }
}
