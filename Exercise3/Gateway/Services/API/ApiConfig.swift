import NeoNetworking

class APIConfigableSample: APIConfigable {
    var debugger: DebuggerConfig = .none
    var host: String = "https://www.themealdb.com/"
}

class APIConfigableSampleForLogin: APIConfigable {
    var debugger: DebuggerConfig = .none
    var host: String = "https://api.sultannews.net/"
}

class APIConfigableSampleForUserInfo: APIConfigable {
    var debugger: DebuggerConfig = .none
    var host: String = "https://api.imgur.com/3/"
}
