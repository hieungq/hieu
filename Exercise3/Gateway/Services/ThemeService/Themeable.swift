import UIKit
protocol Themeable {
    var topBarTintColor: UIColor { get }
    var topBarShadowColor: UIColor { get }
    var topBarImage: UIImage { get }
    var topBarRightButtonImage: UIImage { get }
    var versionTextFont: UIFont { get }
    var versionTextColor: UIColor { get }
    var navigationBarTitleFont: UIFont { get }
    var navigationTitleColor: UIColor { get }
    var navigationBackButtonImage: UIImage { get }
    var navigationBackgroundColor: UIColor { get }
    var appBackgroundColor: UIColor { get }
    
    func applyToTopBar(for view: UIViewController, withTitle title: String)
    func setupRightButtonTopBar(for view: UIViewController, withTitle title: String, action: Selector)
    
    func applyToNavigationBar(for view: UIViewController, withTitle title: String, withHeight height: CGFloat)
    func setupRightButtonToNavigationBar(for view: UIViewController, action: Selector)
    func setupBackButtonToNavigationBar(for view: UIViewController, action: Selector)
    
}
