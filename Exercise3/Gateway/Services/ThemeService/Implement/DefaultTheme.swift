import UIKit
class DefaultTheme: Themeable {
    var topBarTintColor: UIColor = #colorLiteral(red: 0.9999025464, green: 1, blue: 0.9998814464, alpha: 1)
    var topBarImage: UIImage = #imageLiteral(resourceName: "naviImage")
    var topBarRightButtonImage: UIImage = #imageLiteral(resourceName: "rightButton-img")
    var topBarShadowColor: UIColor = UIColor.lightGray
    var versionTextFont: UIFont = UIFont.appFontMedium.withSize(12)
    var versionTextColor: UIColor = .versionTextColor
    var navigationBarTitleFont: UIFont = UIFont.appFontMedium.withSize(15)
    var navigationTitleColor: UIColor = .baseTextColor
    var navigationBackButtonImage: UIImage = #imageLiteral(resourceName: "back")
    var appBackgroundColor: UIColor = .backgroundColor
    var navigationBackgroundColor: UIColor = .naviBarColor
    
    var navigationBarView: AppNavigationBar?
    var topBarRightButtonItem = UIBarButtonItem()
    
    func applyToTopBar(for view: UIViewController, withTitle title: String = "") {
        let navigationController = view.navigationController
        
        if title != "" {
            view.title = title
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.appFontMedium.withSize(20)]
        } else {
            view.navigationItem.titleView = navigationTitleView(view)
        }
        
        navigationController?.navigationBar.layer.masksToBounds = false
        navigationController?.navigationBar.layer.shadowColor = topBarShadowColor.cgColor
        navigationController?.navigationBar.layer.shadowOpacity = 0.8
        navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 3)
        navigationController?.navigationBar.layer.shadowRadius = 2
        navigationController?.navigationBar.barTintColor = topBarTintColor
    }
    
    func navigationTitleView(_ view: UIViewController) -> UIImageView {
        let imageView = UIImageView(frame: CGRect(x: 0,
                                                  y: 0,
                                                  width: topBarImage.size.width,
                                                  height: topBarImage.size.height))
        imageView.contentMode = .scaleAspectFit
        imageView.image = topBarImage
        return imageView
    }
    
    func setupRightButtonTopBar(for view: UIViewController, withTitle title: String = "", action: Selector) {
        guard let size = view.navigationController?.navigationBar.frame else { return }
        let tapGesture = UITapGestureRecognizer(target: view, action: action)
        if title != "" {
            let label = UILabel()
            let rightButtonView = UIView(frame: CGRect(x: 0, y: 0, width: size.height - 8, height: size.height - 8))
            rightButtonView.layer.cornerRadius = (size.height - 8) / 2
            rightButtonView.layer.borderWidth = 1
            rightButtonView.layer.borderColor = UIColor.baseTextColor.cgColor
            rightButtonView.addSubview(label)
            label.frame = rightButtonView.bounds
            label.font = UIFont.appFontRegular.withSize(8)
            label.textAlignment = .center
            label.text = title
            rightButtonView.addGestureRecognizer(tapGesture)
            topBarRightButtonItem.customView = rightButtonView
        } else {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: size.height - 8, height: size.height - 8))
            imageView.image = topBarRightButtonImage
            imageView.contentMode = .scaleToFill
            imageView.addGestureRecognizer(tapGesture)
            topBarRightButtonItem.customView = imageView
        }
        view.navigationItem.rightBarButtonItem = topBarRightButtonItem
    }
    
    func applyToNavigationBar(for view: UIViewController,
                              withTitle title: String,
                              withHeight height: CGFloat) {
        let naviBarViewY = view.navigationController?.getNavigationBarFrame(for: view)
        let width = UIScreen.main.bounds.width
        
        navigationBarView = AppNavigationBar(frame: CGRect(x: 0, y: naviBarViewY?.height ?? 0, width: width, height: height))
        navigationBarView?.setupTitleLabel(title: title)
        
        view.view.addSubview(navigationBarView ?? UIView())
        
    }
    
    func setupRightButtonToNavigationBar(for view: UIViewController, action: Selector) {
        navigationBarView?.setupRightButton(for: view, withAction: action)
    }
    
    func setupBackButtonToNavigationBar(for view: UIViewController, action: Selector) {
        navigationBarView?.setupBackButton(for: view, withAction: action)
    }
}
