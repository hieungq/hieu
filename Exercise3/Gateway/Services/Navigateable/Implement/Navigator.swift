import Foundation
import UIKit

public struct ViewKey {
    public let name: String
    public init(_ name: String) {
        self.name = name
    }
}

public class Navigator: Navigateable {
    public weak var viewController: UIViewController?
    required public init(viewController: UIViewController) {
        self.viewController = viewController
    }
}
