import RealmSwift
import KeychainSwift

let keychain = KeychainSwift()
var privateSecretKey: Data?

extension Realm {
    public static var secretKey: Data? {
        get {
            if privateSecretKey == nil {
                privateSecretKey = keychain.getData("realmSecret")
            }
            return privateSecretKey
        }
        set {
            if let hasValue = newValue {
                keychain.set(hasValue, forKey: "realmSecret")
            } else {
                keychain.delete("realmSecret")
            }
            privateSecretKey = newValue
        }
    }

    public static var shared: Realm? = {
        do {
            return try Realm.setup()
        } catch {
            do {
                if let fileURL = Realm.Configuration.defaultConfiguration.fileURL {
                    try? FileManager.default.removeItem(at: fileURL)
                }
                Logger.error(message: error.localizedDescription)
                return try Realm.setup()
            } catch {
                Logger.error(message: error.localizedDescription)
                return try? Realm()
            }
        }
    }()
    
    /// Inside your application(application:didFinishLaunchingWithOptions:)
    /// - Parameters:
    ///   - schemaVersion: Set the new schema version. This must be greater than the previously used version (if you've never set a schema version before, the version is 0).
    ///   - migrationBlock: Set the block which will be called automatically when opening a Realm with a schema version lower than the one set above
    public static func migrations(schemaVersion: UInt64 = 1, migrationBlock: ((_ migration: Migration, _ oldVersion: UInt64) -> Void)? = nil) {
        let config = Realm.Configuration(
            schemaVersion: schemaVersion,
            migrationBlock: { migration, oldSchemaVersion in
                if oldSchemaVersion < schemaVersion {
                    migrationBlock?(migration, oldSchemaVersion)
                }
            })
        Realm.Configuration.defaultConfiguration = Realm.getConfigWithEncryptionKey(with: config)
    }
    
    internal static func getConfigWithEncryptionKey(with configuration: Configuration) -> Configuration {
        var config = configuration
        var secretKey: Data
        if let key = Realm.secretKey {
            secretKey = key
        } else {
            var key = Data(count: 64)
            _ = key.withUnsafeMutableBytes { bytes in
                SecRandomCopyBytes(kSecRandomDefault, 64, bytes)
            }
            secretKey = key
            Realm.secretKey = key
        }
        config.encryptionKey = secretKey
        config.fileURL = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first?.appendingPathComponent("default.realm")
//        config.deleteRealmIfMigrationNeeded = true
        
        guard let url = config.fileURL else { return config}
        let folderPath = url.deletingLastPathComponent().path
        let fileAttribute = FileAttributeKey(rawValue: FileAttributeKey.protectionKey.rawValue)
        let attributes = [fileAttribute: FileProtectionType.none]
        try? FileManager.default.setAttributes(attributes, ofItemAtPath: folderPath)
        return config
    }
    
    // Init realm with encryption: https://realm.io/docs/swift/latest/#encryption
    internal static func setup() throws -> Realm {
        let config = Realm.getConfigWithEncryptionKey(with: Realm.Configuration.defaultConfiguration)
        return try Realm(configuration: config)
    }
}
