import RealmSwift

class RealmStorateService {
    
    var realm: Realm? {
        return Realm.shared
    }
    
    func migrations(schemaVersion: UInt64 = 1, migrationBlock: ((_ migration: Migration, _ oldVersion: UInt64) -> Void)? = nil) {
        Realm.migrations(schemaVersion: schemaVersion, migrationBlock: migrationBlock)
    }
    
    func write(_ body: () -> Void) {
        autoreleasepool {
            let realm = self.realm
            try? realm?.write(body)
        }
    }
    
    func get<Element: Object>(withId id: String) -> Element? {
        guard let realm = realm else { return nil }
        let result = realm.objects(Element.self)
            .filter("id = '\(id)'")
                          .first
        return result
    }
    
    func getAll<Element: Object>() -> [Element]? {
        guard let realm = realm else { return nil }
        let results = realm.objects(Element.self)
        if results.isEmpty {
            return nil
        }
        var data = [Element]()
        for item in results {
            data.append(item)
        }
//        print("Realm is located at:", realm.configuration.fileURL!)
        return data
    }
    
    func add<Element: Object>(_ entity: Element) {
        guard let realm = realm else { return }
        autoreleasepool {
            realm.beginWrite()
            realm.add(entity, update: .modified)
            try? realm.commitWrite()
        }
    }
    
    func add<Element: Object>(_ entities: [Element]) {
        if entities.isEmpty {
            return
        }
        guard let realm = realm else { return }
        autoreleasepool {
            realm.beginWrite()
            realm.add(entities, update: .modified)
            try? realm.commitWrite()
        }
    }
    
    func delete<Element: Object>(_ entity: Element) {
        guard let realm = realm else { return }
        autoreleasepool {
            realm.beginWrite()
            realm.delete(entity)
            try? realm.commitWrite()
        }
    }
    
    func delete<Element: Object>(_ entities: [Element]) {
        if entities.isEmpty {
            return
        }
        guard let realm = realm else { return }
        autoreleasepool {
            realm.beginWrite()
            realm.delete(entities)
            try? realm.commitWrite()
        }
    }
    
    func delete<Element: Object>(_ entities: Results<Element>) {
        if entities.isEmpty {
            return
        }
        guard let realm = realm else { return }
        autoreleasepool {
            realm.beginWrite()
            realm.delete(entities)
            try? realm.commitWrite()
        }
    }
    
    func deleteAll() {
        guard let realm = realm else { return }
        autoreleasepool {
            realm.beginWrite()
            realm.deleteAll()
            try? realm.commitWrite()
        }
    }
}
