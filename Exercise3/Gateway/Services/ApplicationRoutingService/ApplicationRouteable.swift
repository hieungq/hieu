import UIKit
protocol AppplicationRouteable {
    var window: UIWindow { get }
    func setRoot(view: UIViewController)
    init(window: UIWindow, initialViewController: UIViewController)
}
class AppplicationRoute: AppplicationRouteable {
    private(set) var window: UIWindow
    required init(window: UIWindow = UIWindow(), initialViewController: UIViewController) {
        self.window = window
        self.setRoot(view: initialViewController)
    }
    func setRoot(view: UIViewController) {
        UIView.transition(with: window, duration: 0.22, options: .transitionFlipFromRight, animations: {
            let oldState = UIView.areAnimationsEnabled
            UIView.setAnimationsEnabled(false)
            self.window.rootViewController = view
            UIView.setAnimationsEnabled(oldState)
        })
    }
}
