import UIKit

//emergencyContacts
final class AlertVC: UIViewController {

    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var tittleLabel: UILabel!
    @IBOutlet weak var warningLabel: UILabel!
    @IBOutlet weak var warningImageView: UIImageView!
    @IBOutlet weak var teacherImageView: UIImageView!
    @IBOutlet weak var teacherNameLabel: UILabel!
    @IBOutlet weak var reasonLabel: UILabel!
    @IBOutlet weak var childrenLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    convenience init(titleLabel title: String, warningLabel warning: String, warningImage: UIImage, teacherImage: UIImage, teacherName name: String) {
        self.init()
    }
}
