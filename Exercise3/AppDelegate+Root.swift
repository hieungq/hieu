import UIKit

extension AppDelegate {
    
    func configRoot() {
        let vc = Screen2VC()
        let rootVC = UINavigationController(rootViewController: vc)
        
        if let window = self.window {
            _ = ServiceFacade.applicationService.applicationRoute(from: window, initialViewController: rootVC)
        }
    }
}
