import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        self.window = UIWindow(frame: UIScreen.main.bounds)
        guard let window = window else { return true}
        ServiceFacade.registerDefaultService(from: window)
        configRoot()

        self.window?.makeKeyAndVisible()
        return true
    }
}
