import UIKit

class Screen2VC: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        theme?.applyToTopBar(for: self)
        theme?.setupRightButtonTopBar(for: self, action: #selector(didTouchRightButtonTopBar))
        
        theme?.applyToNavigationBar(for: self, withTitle: "新規登録", withHeight: 49)
        theme?.setupRightButtonToNavigationBar(for: self, action: #selector(didTouchRightButtonNavigationBar))
        theme?.setupBackButtonToNavigationBar(for: self, action: #selector(didTouchBackButtonNavigationBar))
        
        setupVersionLabel()
    }
    
    @objc private func didTouchRightButtonTopBar() {
        print("didTouchRightButtonTopBar")
    }
    
    @objc private func didTouchRightButtonNavigationBar() {
        print("didTouchRightButtonNavigationBar")
    }
    
    @objc private func didTouchBackButtonNavigationBar() {
        print("didTouchBackButtonNavigationBar")
    }
}
