import UIKit

class BaseViewController: UIViewController {
    
    var theme = ServiceFacade.getService(DefaultTheme.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = theme?.appBackgroundColor
    }
    
    func setupVersionLabel() {
        let versionLabel = UILabel()
        versionLabel.translatesAutoresizingMaskIntoConstraints = false
        versionLabel.textAlignment = .center
        versionLabel.text = "バージョン 1.0.0.0"
        versionLabel.font = theme?.versionTextFont
        versionLabel.textColor = theme?.versionTextColor
        view.addSubview(versionLabel)
        let versionLabelConstraint = [
            versionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            versionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            versionLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16)
        ]
        NSLayoutConstraint.activate(versionLabelConstraint)
    }
}
