import UIKit

class BaseNaviController: UINavigationController {
    
    var theme = ServiceFacade.getService(DefaultTheme.self)
    
    public override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        setupNavigation(view: rootViewController)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupNavigation(view: UIViewController) {
        let navigationController = view.navigationController
        
        view.navigationItem.titleView = navigationTitleView()
        navigationController?.navigationBar.layer.masksToBounds = false
        navigationController?.navigationBar.layer.shadowColor = theme?.topBarShadowColor.cgColor
        navigationController?.navigationBar.layer.shadowOpacity = 0.8
        navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 3)
        navigationController?.navigationBar.layer.shadowRadius = 2
        navigationController?.navigationBar.barTintColor = theme?.topBarTintColor
    }

    func navigationTitleView() -> UIImageView {
        let size = navigationBar.frame.size
        let imageView = UIImageView(frame: CGRect(x: 0,
                                                  y: 0,
                                                  width: size.width,
                                                  height: size.height))
        imageView.contentMode = .scaleAspectFit
        imageView.image = theme?.topBarImage
        return imageView
    }
}
